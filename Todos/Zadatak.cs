﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todos {
    // Zadatak.cs
    class Zadatak {
        public Zadatak(object id, object nazivZadatka, object zavrseno, object izradeno) {
            Id = (int)id;
            NazivZadatka = (string)nazivZadatka;
            Zavrseno = (bool)zavrseno;
            Izradeno = (DateTime)izradeno;
        }
        public Zadatak(string nazivZadatka, bool zavrseno) {
            NazivZadatka = nazivZadatka;
            Zavrseno = zavrseno;
            Izradeno = DateTime.Now;
        }

        public override string ToString() {
            return string.Format("{0} ({1})", NazivZadatka, Izradeno);
        }

        public int Id { get; set; }
        public string NazivZadatka { get; set; }
        public bool Zavrseno { get; set; }
        public DateTime Izradeno { get; set; }
    }
}
